#include <iostream>
using namespace std;
#include <string>
#include "ejercicio13.h"


void cargarVector13(int entero, int vector13[])
{
    vector13[0] = entero / 10000;
    vector13[1] = entero % 10000 / 1000;
    vector13[2] = entero % 10000 % 1000 / 100;
    vector13[3] = entero % 10000 % 1000 % 100 / 10;
    vector13[4] = entero % 10000 % 1000 % 100 % 10;
}

void mostrarVector13(int *vector13)
{
    for (int i = 0; i < 5; i++){
        cout << vector13[i] << "  ";
    }

}

