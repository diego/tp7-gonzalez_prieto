#include <iostream>
#include "ejercicio13.h"
#include "ejercicio14.h"
#include "ejercicio15.h"
#include "ejercicio8-B.h"

using namespace std;

int main()
{
    cout << "*** Ejercicio 13 ***" << endl << endl;
    /*
    Hacer una función que reciba un número entero de hasta 5 cifras y un vector de enteros de cinco elementos
    y descomponga las cifras del número y las asigne a cada elemento del vector. Por ejemplo:
        Si recibe 45323 como parámetro, el vector quedará {4, 5, 3, 2, 3}
        Si recibe 390 como parámetro, el vector quedará {0, 0, 3, 9, 0}
    */
    int a = 36182;
    int vec[5] = {};

    cargarVector13(a, vec);
    cout << "Entero: " << a << endl<< endl;
    cout << "Vector:\t";
    mostrarVector13(vec);
    cout << endl << endl << endl;


    cout << "*** Ejercicio 14 ***" << endl << endl;
    /*
    Ídem al ejercicio 6 pero debe devolver la cantidad de veces que el valor buscado se encuentra dentro del vector.

    Ejercicio 6:
    Hacer una función que reciba un número entero llamado valor_buscado, un vector de números enteros y su tamaño //
    */

    int valor_buscado = 10;
    int TAMVECTOR14 = 10;
    int vector14[TAMVECTOR14] = {1, 10, 8, 7, 19, 10, 5, 14, 2, 1};


    cout << "Cantidad de veces encontrado: " << contarOcurrencias(valor_buscado, vector14, TAMVECTOR14);

    cout << endl << endl << endl;


    cout << "*** Ejercicio 15 ***" << endl << endl;
    /*
        Publicar en la cafetería del campus virtual la cantidad de números entre el 1 y 99999 que tienen más 3 que 4
        pero que al menos tenga un 4 y ningún 5.
    */

    cout << "Son " << contarCondicion(1, 99999) << " números que cumplen con las condiciones.";
    cout << endl << endl << endl;

    cout << "*** Ejercicio 8 - Guía extra ***" << endl << endl;

    /*
    Hacer una rutina llamada ecuacionCuadratica que permita obtener las dos
    soluciones de una ecuación cuadrática del tipo ax²+bx+c (sólo cuando el
    discriminante sea positivo). La rutina deberá recibir los valores para a, b y c. Luego
    calculará las dos soluciones.
    Utilizar la siguiente fórmula:
    Para calcular la raíz de un número deberán utilizar la función sqrt de la librería
    math.h
    Tener en cuenta que:
    − El valor a no puede ser cero Indicarlo con un mensaje de error. →
    − El valor a calcular su raíz cuadrada deberá ser siempre positivo Indicarlo →
    con un mensaje de error.
    */

    float a15 = 2, b15 = 4, c15 = 1;

    cout << "Resultado de la ecuación cuadrática:" << endl << endl;

    ecuacionCuadratica(a15, b15, c15);

    return 0;
}
