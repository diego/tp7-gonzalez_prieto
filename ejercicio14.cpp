#include <iostream>
using namespace std;
#include "ejercicio14.h"

int contarOcurrencias(int valor_buscado, int vector14[], int tamVector)
{
    int cantidad = 0;
    for (int i = 0; i < tamVector; i++){
        if (valor_buscado == vector14[i]){
            cantidad++;
        }
    }
    return cantidad;
}
