#include <iostream>
using namespace std;
#include "ejercicio15.h"


int obtenerNumeros(int n, int vec[])
{
    vec[0] = n / 10000;
    vec[1] = n % 10000 / 1000;
    vec[2] = n % 10000 % 1000 / 100;
    vec[3] = n % 10000 % 1000 % 100 / 10;
    vec[4] = n % 10000 % 1000 % 100 % 10;
}

int contarCifras(int vec[], int cifra)
{
    int contador = 0;
    for (int i = 0; i < 5; i++)
        {
            if (cifra == vec[i])
                {
                    contador++;
                }
        }
        return contador;
}


int contarCondicion(int desde, int hasta)
{
    int cantidad = 0;
    for (int i = desde; i <= hasta; i++)
        {
            int numeros[5] {};
            obtenerNumeros(i, numeros);

            if (                contarCifras(numeros, 4) > 0
                                &&
                                contarCifras(numeros, 5) == 0
                                &&
                                contarCifras(numeros, 4) < contarCifras(numeros, 3)
               )
                {
                    cantidad++;
                }
        }

    return cantidad;
}
