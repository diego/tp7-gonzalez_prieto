#include <iostream>
using namespace std;
#include "ejercicio8-B.h"
#include <math.h>

void ecuacionCuadratica(float a, float b, float c)
{
    if (a == 0)
        {
            cout << "El valor de 'a' no puede ser 0." << endl;
            return;
        }

    float discriminante = pow(b, 2) - 4 * a * c;
    if (discriminante < 0)
        {
            cout << "El discriminante es negativo, no hay solución." << endl;
            return;
        }
    else if (discriminante == 0) {
            cout << "Un solo resultado: " << -b/(2*a) << endl;
    } else {
            cout << "Hay dos resultados: " << endl << endl;
            cout << (-b + sqrt(discriminante))/(2*a)  << " y " << (-b - sqrt(discriminante))/(2*a) << endl;
    }
}
